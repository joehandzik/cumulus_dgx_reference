# NVIDIA Cumulus Linux Reference Configurations for DGX Pods

[![production](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/badges/production/pipeline.svg?key_text=production&key_width=75)](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/pipelines?page=1&scope=all&ref=production)
[![development](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/badges/development/pipeline.svg?key_text=development&key_width=85)](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/pipelines?page=1&scope=all&ref=development)
[![License](https://img.shields.io/badge/License-Apache%202.0-83389B.svg)](https://opensource.org/licenses/Apache-2.0)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)
[![Slack Status](https://img.shields.io/badge/Slack-3400+-F1446F)](https://slack.cumulusnetworks.com)
[![Code of Conduct](https://img.shields.io/badge/Contributing-Code%20of%20Conduct-1EB5BD)](https://docs.cumulusnetworks.com/contributor-guide/#contributor-covenant-code-of-conduct)

<img src="https://www.ansible.com/hubfs/2016_Images/Assets/Ansible-Mark-Large-RGB-BlackOutline.png" height="150" title="Ansible" align="right" /> 
<img src="https://gitlab.com/cumulus-consulting/goldenturtle/cldemo2/-/raw/master/documentation/images/cumulus-logo.svg" height="150" title="Cumulus Networks" align="right" /> 

The following repository provides reference configurations for Cumulus Linux to be used for compute and storage networks for AI workloads on [NVIDIA DGX systems](https://www.nvidia.com/en-us/data-center/dgx-systems/).

- [NVIDIA Cumulus Linux Reference Configurations for DGX Pods](#NVIDIA-cumulus-linux-reference-configurations-for-dgx-pods)
  - [Topologies](#topologies)
  - [References](#references)

## Topologies
[NCCL Ring 2 Node Topology](inventories/dgx_2node/)  
[NCCL Ring 4 Node Topology](inventories/dgx_4node/)  
[NCCL Ring 8 Node Topology](inventories/dgx_8node/)  
[Collapsed 2 Node Topology](inventories/dgx_2node_collapsed/)  
[CLOS 8 Node Topology](inventories/dgx_8node_clos/)

## References
[Bill of Materials](https://docs.google.com/spreadsheets/d/1v3W2wq8TeIcrs1ipd9wk111Quak2tdct4n6xNVU3RDQ/edit?usp=sharing)