## DGX 2 Node Collapsed Topology
### Topology
![topology](topology/topology.svg)

### Cabling Map
![cabling map](topology/cabling.png)

<!-- AIR:tour -->
### Configurations
#### gpu-stg-leaf01
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_2node_collapsed/config/gpu-stg-leaf01/interfaces)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_2node_collapsed/config/gpu-stg-leaf01/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_2node_collapsed/config/gpu-stg-leaf01/datapath.conf)

#### gpu-stg-leaf02
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_2node_collapsed/config/gpu-stg-leaf02/interfaces)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_2node_collapsed/config/gpu-stg-leaf02/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_2node_collapsed/config/gpu-stg-leaf02/datapath.conf)
<!-- AIR:tour -->